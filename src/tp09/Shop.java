package tp09;

import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class Shop {
	private HashMap<String, Article> catalog = new HashMap<String, Article>();
	public boolean addArticle(Article a) {
		this.catalog.putIfAbsent(a.getIdRef(),a);
		return true;
	}
	public boolean addArticle(Collection<Article> c) {
		for(Article art : c) {
			this.catalog.putIfAbsent(art.getIdRef(),art);
		}
		return true;
	}
	public String toString() {
		StringBuilder res = new StringBuilder();
		for(Article l: this.catalog.values()) res.append(l.toString()).append("\n");
		return res.toString();
	}
	public Article getArticle(String idRef) {
		return this.catalog.get(idRef);
	}
	public List<PerishableArticle> getPerishables(){
		List<PerishableArticle> l = new ArrayList<PerishableArticle>();
		for(Article a : this.catalog.values()) {
			if(a.isPerishable()) {
				l.add((PerishableArticle) a);
			}
		}
		return l;
	}
	public void discountPerishable(LocalDate threshold, double rate) {
		List<PerishableArticle> l = this.getPerishables();
		for(PerishableArticle a : l) {
			if((a.getDeadLine().isBefore(threshold))) {
				a.setSalePrice(a.getSalePrice()*rate);
			}
		}
	}
}
