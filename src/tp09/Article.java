package tp09;

public class Article {
	final private String idRef;
	final private String label;
	final private double PURCHASE_PRICE;
	private double salePrice;
	public Article(String idRef, String label, double purchasePrice, double salePrice) {
		this.idRef = idRef;
		this.label = label;
		this.PURCHASE_PRICE = purchasePrice;
		this.salePrice = salePrice;
	}
	public Article(String idRef, String label, double purchasePrice) {
		this.idRef = idRef;
		this.label = label;
		this.PURCHASE_PRICE = purchasePrice;
		this.salePrice = purchasePrice*1.2;
	}
	public double getSalePrice() {
		return salePrice;
	}
	public double getMargin() {
		return this.salePrice-PURCHASE_PRICE;
	}
	public String getIdRef() {
		return idRef;
	}
	public String getLabel() {
		return label;
	}
	public void setSalePrice(double salePrice) {
		this.salePrice = salePrice;
	}
	@Override
	public String toString() {
		return "Article [" + idRef + ":" + label + "=" + PURCHASE_PRICE + "e/"+salePrice+"e]";
	}
	public boolean isPerishable() {
		return false;
	}
	
}
