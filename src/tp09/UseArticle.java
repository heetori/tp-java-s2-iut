package tp09;

import java.util.ArrayList;
import java.util.Collection;

public class UseArticle {
	public static void main(String[] args) {
		Article a1 = new Article("chaise1","chaise",12.3);
		Article a2 = new Article("bureau1","bureau",120.3);
		PerishableArticle pa1 = new PerishableArticle("Saumon1","Saumon",2);
		PerishableArticle pa2 = new PerishableArticle("SteakCharal1","Steak Hach�",2);
		PerishableArticle pa3 = new PerishableArticle("banana1","banane",2);
		ArrayList<Article> pa = new ArrayList<Article>();
		pa.add(pa1);pa.add(pa2);pa.add(pa3);
		Collection<Article> paL = pa;
		
		Shop aldi = new Shop();
		aldi.addArticle(a1);
		aldi.addArticle(a2);
		aldi.addArticle(paL);
		System.out.println(aldi.toString());
		
	}
}
