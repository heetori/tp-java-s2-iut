package tp09;

public class UseLibrary {
	public static void main(String[] args) {
		Book b1 = new Book("H2G2", "The Hitchhiker s Guide to the Galaxy","D. Adams",1979);
		Book b2 = new Book("FLTL", "Flatland","E.Abbott Abbott",1884);
		Book b3 = new Book("42", "The Restaurant at the End of the Universe","D. Adams",1980);
		Library bib = new Library();
		bib.addBook(b1);
		bib.addBook(b2);
		bib.addBook(b3);
		
		
		System.out.println("A specific book :");
		System.out.println(bib.getBook("H2G2").toString());
		System.out.println("Display of the library :");
		System.out.println(bib.toString());
		
		
		System.out.println();
		System.out.println("Borrowings list");
		System.out.println("Initialy -->");
		System.out.print(bib.getBook("FLTL").isAvailable());
		bib.borrow("H2G2", 548);
		bib.borrow("42", 549);
		System.out.print(" --> " + bib.borrowings());
		System.out.println();
		System.out.print(bib.getBook("H2G2").isAvailable());
		System.out.print("--> " + bib.borrowings());
		System.out.println();
		System.out.print(bib.getBook("FLTL").isAvailable());
		System.out.print("--> " + bib.borrowings());
		bib.giveBack("H2G2");
		bib.borrow("FLTL", 549);
		System.out.print(bib.borrowings());
		
		
		ComicBook c1 = new ComicBook("LeuG","L�onard est un G�nie","Bob de Groot",1977," Turk");
		bib.addBook(c1);
		System.out.println();
		System.out.println();
		System.out.println("A specific book :");
		System.out.println(bib.getBook("H2G2").toString());
		System.out.println(bib.getBook("LeuG").toString());
		System.out.println();
		bib.giveBack("FLTL");
		bib.borrow("H2G2", 548);
		System.out.print(bib.getBook("FLTL").isAvailable());
		System.out.print(" --> " + bib.borrowings());
		System.out.println();
		System.out.print(bib.getBook("FLTL").isAvailable());
		System.out.print(" --> " + bib.borrowings());
		bib.giveBack("H2G2");
		bib.borrow("LeuG", 5155);
		System.out.print(bib.borrowings());
		System.out.println();
		System.out.println();
		System.out.println("A specific book :");
		System.out.println(bib.getBook("H2G2").toString());
		System.out.println(bib.getBook("LeuG").toString());
		bib.giveBack("H2G2");
		bib.giveBack("42");
		bib.giveBack("FLTL");
		bib.giveBack("LeuG");
		bib.borrow("LeuG", 5155);
		bib.borrow("FLTL", 5155);
		System.out.println();
		System.out.println(bib.getBook("42").getGiveBackDate());
		System.out.println(bib.getBook("LeuG").getGiveBackDate());
		
	}
}
