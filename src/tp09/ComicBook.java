package tp09;

import java.time.LocalDate;

public class ComicBook extends Book {
	final private String illustrator;
	private LocalDate borrowingDate;
	public ComicBook(String code, String title, String author, int publicationYear, String illustrator) {
		super(code, title, author, publicationYear);
		this.illustrator = illustrator;
		this.borrowingDate = LocalDate.now();
	}
	public String getIllustrator() {
		return illustrator;
	}
	@Override
	public String toString() {
		return "ComicBook [code=" + getCode() + ", title=" + getTitle() + ", author=" + getAuthor() + ", publicationYear=" + getPublicationYear() + ", illustrator=" + illustrator + "]";
	}
	public int getDurationMax() {
		return 5;
	}
	public LocalDate getGiveBackDate() {
		return this.borrowingDate.plusDays(this.getDurationMax());
	}
}
