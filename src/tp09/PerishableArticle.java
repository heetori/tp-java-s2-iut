package tp09;

import java.time.LocalDate;

public class PerishableArticle extends Article{
	final private LocalDate deadLine;
	public PerishableArticle(String idRef, String label, double pA, double pV, LocalDate dl) {
		super(idRef, label, pA, pV);
		this.deadLine = dl;
	}
	public PerishableArticle(String idRef, String label, double pA, LocalDate dl) {
		super(idRef, label, pA);
		this.deadLine = dl;
	}
	public PerishableArticle(String idRef, String label, double pA, double pV) {
		super(idRef, label, pA, pV);
		this.deadLine = LocalDate.now().plusDays(10);
	}
	public PerishableArticle(String idRef, String label, double pA) {
		super(idRef, label, pA);
		this.deadLine = LocalDate.now().plusDays(10);
	}
	public LocalDate getDeadLine() {
		return deadLine;
	}
	public String toString() {
		double purch = (this.getSalePrice())-(this.getMargin());
		return "PerishableArticle ["+this.getIdRef()+":"+this.getLabel()+"="+this.getSalePrice()+"e/"+purch+"e-->"+this.deadLine+"]";
	}
	public boolean isPerishable() {
		return true;
	}
}
