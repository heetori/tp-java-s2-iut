package tp09;

import java.util.HashMap;

public class Library {
	private HashMap<String,Book> catalog = new HashMap<String,Book>();
	public Book getBook(String code) {
		return this.catalog.get(code);
	}
	public boolean addBook(Book b) {
		this.catalog.putIfAbsent(b.getCode(), b);
		return true;
	}
	public boolean removeBook(String aCode) {
		this.catalog.remove(aCode);
		return true;
	}
	public boolean removeBook(Book b) {
		this.catalog.remove(b.getCode());
		return true;
	}
	public String toString() {
		String res ="";
		for(Book b : this.catalog.values()) {
			res += b+"\n";
		}
		return res;
	}
	public String borrowings() {
		String res = "(";
		for(Book b : this.catalog.values()) {
			if(!b.isAvailable()) {
				res += b.getCode()+"--";
			}
		}
		return res.substring(0, res.length()-2)+")";
	}
	public boolean borrow(String code, int borrower) {
		return this.catalog.get(code).borrow(borrower);
	}
	public boolean giveBack(String code) {
		return this.catalog.get(code).giveBack();
	}
}