package tp09;

import java.time.LocalDate;

public class Book {
	private String code;
	private int borrow;
	private LocalDate borrowingDate;
	final private String title;
	final private String author;
	final private int publicationYear;
	
	public Book(String code, String title, String author, int publicationYear) {
		this.code = code;
		this.title = title;
		this.author = author;
		this.publicationYear = publicationYear;
		this.borrow = -1;
		this.borrowingDate = LocalDate.now();
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public int getPublicationYear() {
		return publicationYear;
	}
	@Override
	public String toString() {
		return "Book [code=" + code + ", title=" + title + ", author=" + author + ", publicationYear=" + publicationYear
				+ "]";
	}
	public boolean borrow(int borrower) {
		if(this.isAvailable()) {
			this.borrow = borrower; return true;
		} else return false;
	}
	public boolean giveBack() {
		if(this.borrow < 0) return false;
		else {
			this.borrow = -1;
			return true;
		}
	}
	public boolean isAvailable() {
		if(this.borrow < 0) return true;
		else return false;
	}
	public int getDurationMax() {
		return 10;
	}
	
	public LocalDate getGiveBackDate() {
		return this.borrowingDate.plusDays(this.getDurationMax());
	}
}
