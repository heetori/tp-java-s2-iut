package tp02;
import java.util.Scanner;

public class NDicePlayerGame {
	DicePlayer[] game;
	NDicePlayerGame(int nbJoueur){
		this.game = new DicePlayer[nbJoueur];
		for(int i = 1; i <= nbJoueur;i++) {
			game[i-1] = new DicePlayer("Joueur " + i);
		}
	}
	
	DicePlayer[] winner(){
		DicePlayer [] winner = new DicePlayer[this.game.length];
		int nbW = 0;
		for(int i = 0; i < game.length; i++) {
			if(winner[0] == null ) {
				winner[0] = game[i];
			} else if(game[i].nbDiceRolls == winner[0].nbDiceRolls) {
				nbW++;
				winner[nbW] = game[i];
			} else if(game[i].nbDiceRolls < winner[0].nbDiceRolls){
				for(int j = 0; j < winner.length; j++) {
					winner[j] = null;
				}
				nbW = 0;
				winner[0] = game[i];
			}
		}
		return winner;
	}
	
	public static void main(String args[]) {
		Scanner scan = new Scanner( System.in );
		int nbJ = scan.nextInt();
		
		Dice de = new Dice(6);
		NDicePlayerGame jeuCourant = new NDicePlayerGame(nbJ);
		for(int i = 0; i < nbJ; i++) {
			while(jeuCourant.game[i].totalValue <= 20) {
				jeuCourant.game[i].play(de);
			}
		}
		DicePlayer[] winner = jeuCourant.winner();
		for(int i = 0; i < winner.length; i++) {
			if(winner[i] != null) {
				System.out.println(winner[i]);
			}
		}
		scan.close();
	}
}
