package tp02;

public class TwoDicePlayerGame {
	DicePlayer j1;
	DicePlayer j2;
	public TwoDicePlayerGame(DicePlayer j1, DicePlayer j2) {
		this.j1 = j1;
		this.j2 = j2;
	}
	DicePlayer winner() {
		if(j1.nbDiceRolls < j2.nbDiceRolls) {
			return j1;
		} else{
			return j2;
		} 
	}
	public static void main(String args[]) {
		DicePlayer Gauthier = new DicePlayer("Gauthier");
		DicePlayer William = new DicePlayer("William");
		TwoDicePlayerGame game = new TwoDicePlayerGame(Gauthier, William);
		Dice de = new Dice(6);
		boolean tour = true;
		while(tour) {
			if(Gauthier.totalValue <= 20) {
				Gauthier.play(de);
			} else if(William.totalValue <= 20){
				William.play(de);
			} else {
				tour = false;
			}
		}
		System.out.println("Le gagnant est : "+game.winner());
	}
}
