package tp02;

public class DicePlayer {
	String name;
	int totalValue;
	int nbDiceRolls;
	
	public DicePlayer(String name){
		if(name != null) {
			this.name = name;
			this.totalValue = 0;
			this.nbDiceRolls = 0;
		}
	}
	void play(Dice dice) {
		dice.roll();
		this.totalValue += dice.value;
		this.nbDiceRolls++;
	}
	
	public String toString() {
		return this.name + ": " + this.totalValue+" points en "+this.nbDiceRolls+" coups.";
	}
}
