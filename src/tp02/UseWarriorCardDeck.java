package tp02;
import java.util.Random;

public class UseWarriorCardDeck {
	public static void main(String [] args) {
		System.out.println("SCENARIO 1");
		WarriorCardDeck deck1 = new WarriorCardDeck();
		WarriorCardDeck deck2 = new WarriorCardDeck();
		deck1.addCard(new WarriorCard("Alice", 7,5));
		deck1.addCard(new WarriorCard("Bruno", 2,6));
		deck2.addCard(new WarriorCard("Clément", 2,4));
		deck2.addCard(new WarriorCard("Dorothée", 3,2));
		System.out.println("Initially :");
		System.out.println(deck1);
		System.out.println(deck2);
		deck1.shuffle();
		deck2.shuffle();
		System.out.println("After shuffle");
		System.out.println(deck1);
		System.out.println(deck2);
		System.out.println("One card match :");
		if(deck1.getCard().strength < deck2.getCard().strength ) {
			System.out.println(deck2.getCard() +"is stronger than "+deck1.getCard());
			deck1.pullCard();
		} else {
			System.out.println(deck1.getCard() +"is stronger than "+deck2.getCard());
			deck2.pullCard();
			
		}
		System.out.println("After the first round :");
		System.out.println(deck1);
		System.out.println(deck2);
		System.out.println("\n\nSCENARIO 2");
		String [] names1 = new String [] {"Martin", "Thomas", "Fred"};
		String [] names2 = new String [] {"Gauthier", "Th�on", "William", "Colin"};
		scenario(names1, names2);
	}
	static void scenario(String[] names1, String [] names2) {
		Random r = new Random();
		WarriorCardDeck deck1 = new WarriorCardDeck();
		WarriorCardDeck deck2 = new WarriorCardDeck();
		for(int i = 0; i < names1.length;i++) {
			deck1.addCard(new WarriorCard(names1[i], r.nextInt(11), r.nextInt(11)));
		}
		for(int i = 0; i < names2.length;i++) {
			deck2.addCard(new WarriorCard(names2[i], r.nextInt(11), r.nextInt(11)));
		}
		while(!deck1.isEmpty() && !deck2.isEmpty()) {
			deck1.shuffle();
			deck2.shuffle();
			if(deck1.getCard().strength < deck2.getCard().strength ) {
				System.out.println(deck2.getCard() +"is stronger than "+deck1.getCard());
				deck1.pullCard();
			} else {
				System.out.println(deck1.getCard() +"is stronger than "+deck2.getCard());
				deck2.pullCard();
			}
		}
 		System.out.println("Le ou les gagnants sont :");
		if(deck1.isEmpty()) {
			System.out.println(deck2);
		} else {
			System.out.println(deck1);
		}
	}
}
