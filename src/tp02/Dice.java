package tp02;
import java.util.Random;

public class Dice {
	int numberSides;
	Random rand;
	int value;
	public Dice(int numberSides){
		if(numberSides >= 0) {
			this.numberSides = numberSides;
			rand = new Random();
			this.roll();
		} else {
			this.numberSides = 1;
			rand = new Random();
			this.roll();
		}
	}
	
	void roll() {
		this.value = rand.nextInt(this.numberSides)+1;
	}
	
	public String toString() {
		return "" + this.value;
	}
	
}
