package tp02;

public class OneDicePlayerGame {
	public static void main(String args[]) {
		DicePlayer Gauthier = new DicePlayer("Gauthier");
		Dice de = new Dice(6);
		while(Gauthier.totalValue <= 20) {
			Gauthier.play(de);
		}
		System.out.println(Gauthier);
	}
}
