package tp02;
/**
 * @author Gauthier
 *
 * 
 * */

public class WarriorCard {
	String name;
	int strength;
	int agility;
	/**
	 * Crée une carte complétement spécifiée
	 *  @param name Nom de la carte
	 *  @param s Force de la carte
	 *  @param ag Agilité de la carte
	 * */
	WarriorCard(String name, int s, int ag){
		this.name = name;
		this.strength = s;
		this.agility = ag;
	}
	
	/** Détermine si la carte de guerrier passée en paramètre est 
	 * équivalente a la carte courante uniquement basé sur le nom 
	 * du guerrier répresentée sur la carte
	 * @param other Autre carte a comparer
	 * @return Retour true si les cartes sont pareil sinon renvoie false
	*/
	boolean equals(WarriorCard other) {
		if(this.agility == other.agility && this.strength == other.strength) {
			return true;
		} else {
			return false;	
		}
	}
	/**
	 * @param other Autre carte a comparer
	 * @return Retourne: 0 si les deux guerriers ont une force équivalente;
				+0 si la force du guerrier courant est inférieure à celle de celui passé en paramètre;
				-0 si la force du guerrier courant est supérieure à celle de celui passé en paramètre.
	
	 * */
	int compareStrength(WarriorCard other) {
		return this.strength - other.strength;
	}
	
	/**
	 * @param other Autre carte a comparer
	 * @return Retourne: 0 si les deux guerriers ont une agilité équivalente;
				+0 si l’agilité du guerrier courant est inférieure à celle de celui passé en paramètre;
				-0 si l’agilité du guerrier courant est supérieure à celle de celui passé en paramètre.
	 * */
	int compareAgility(WarriorCard other) {
		return this.agility - other.agility;
	}
	
	/**
	 * @return Retourne la représentation textuelle d’une carte guerrier, contenant son nom, sa valeur de force et sa valeur d’agilité.
	 * */
	public String toString() {
		return this.name + "[S=" + this.strength + ",A=" + this.agility+"]";
	}
}
