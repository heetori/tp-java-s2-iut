package tp02;

public class Competitor {
	String numberSign;
	int score;
	int time;
	Competitor(int numberSign, int score, int min, int sec){
		if(numberSign >= 1 && numberSign <= 100 && score >= 0 && score <= 50 && min >=0 && min <= 60 && sec >= 0 && sec <=60) {
			this.numberSign = "No"+numberSign;
			this.score = score;
			this.time = (min*60)+sec;
		} else {
			this.numberSign = null;
			this.score = 0;
			this.time = 0;
		}
		
	}
	String display() {
		return ((this.numberSign == null)?"":"["+this.numberSign+", "+this.score+" points,"+this.time+"s]");
	}
	public static void main(String args[]) {
		Competitor tab [] = new Competitor[100];
		tab[0] = new Competitor(1,45,15,20
				);
		tab[1] = new Competitor(2,32,12,45);
		tab[2] = new Competitor(5,12,13,59);
		tab[3] = new Competitor(12,12,15,70);
		tab[4] = new Competitor(32,75,15,20);
		for(int i = 0; i < tab.length;i++) {
			if(tab[i] != null && tab[i].numberSign != null) {
				System.out.println(tab[i].display());
			}
			
		}
	}
}

