package tp07;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import util.PieChart;
public class SuperPieChart {
	public static void main(String [] args) {
		String [] s = {"un","deux","trois","quatre"};
		float [] f = {(float) 1.5,(float) 2.5,(float) 3.5,(float) 4.5};
		SuperPieChart test = new SuperPieChart("Test", s,f);
		System.out.println(test);
	}
	
	
	private PieChart diag;
	
	
	public SuperPieChart(String name, String[] s, float[] f, Color[] c) {
		Map <String, Float> values = new HashMap<String, Float>();
		Map <String, Color> colors = new HashMap<String, Color>();
		for(int i = 0; i < s.length; i ++) {
			values.put(s[i], f[i]);
			colors.put(s[i], c[i]);
		}
		this.diag = new PieChart(name,values,colors);
	}
	
	public SuperPieChart(String name, String[] s, float[] f) {
		Map <String, Float> values = new HashMap<String, Float>();
		Map <String, Color> colors = new HashMap<String, Color>();
		Color c = Color.RED;
		for(int i = 0; i < s.length; i ++) {
			values.put(s[i], f[i]);
			colors.put(s[i], c);
			c = c.darker();
		}
		this.diag = new PieChart(name,values,colors);
	}
	
	public boolean fillIn(String[] s,float[] f) {
		if(s.length != s.length) {
			return false;
		}
		Map <String, Float> values = new HashMap<String, Float>();
		for(int i = 0; i < s.length;i++) {
			values.put(s[i], f[i]);
		}
		return diag.fillIn(values);
	}
	
	public boolean color(String[] s,Color[] c) {
		if(s.length != c.length) {
			return false;
		}
		Map <String, Color> colors = new HashMap<String, Color>();
		for(int i = 0; i < s.length;i++) {
			colors.put(s[i], c[i]);
		}
		return diag.color(colors);
	}
	
	boolean fading(HashMap<String, Float> values) {
		ArrayList <String> tmps = new ArrayList<String>(); 
		ArrayList <Color> tmpc = new ArrayList<Color>(); 
		Color c = Color.RED;
		for(Float f : values.values()) {
			for(int i = 0; i < f; i++) {
				c = c.darker();
			}
			tmpc.add(c);
		}
		for(String s : values.keySet()) {
			tmps.add(s);
		}
		String [] name = new String[tmps.size()];
		Color [] colors = new Color[tmpc.size()];
		for(int i = 0; i < tmps.size(); i++) {
			name[i] = tmps.get(i);
			colors[i] = tmpc.get(i);
		}
		
		return color(name, colors);
	}
}
