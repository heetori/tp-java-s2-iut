package tp07;
import tp04.Task;
import java.time.LocalDate;
import util.BarChart;
import java.util.ArrayList;
import java.util.Random;

public class UseFreeChart {
	public static void main(String [] args) {
		double [] values = new double [100]; 
		Random r = new Random();
		for(int i  = 0; i < values.length; i++) {
			values[i] = r.nextDouble()*101;
		}
		BarChart b = new BarChart("Frequence", "Nombres", "nbOcurence",values, values.length);
		System.out.println(b);
		ArrayList <Task> taksklist = generateTasks();
		barChartTask(taksklist);
		
	}
	public static ArrayList<Task> generateTasks() {
		Random r = new Random();
		ArrayList<Task> tmp = new ArrayList<Task>();		
		for(int mois = 1; mois <= 12;mois++) {
			int nbTask = r.nextInt(5);
			for(int i = 0; i < nbTask; i ++) {
				int taskLong = r.nextInt(9)+1;
				tmp.add(new Task("Task"+i, LocalDate.now(), LocalDate.of(2019, mois, 1), taskLong));
			}
		}
		return tmp;
	}
	
	public static void barChartTask(ArrayList<Task> l) {
		double values[] = new double[l.size()]; 
		for(int i = 0;i < l.size(); i++) {
			values[i] = (double)l.get(i).getDeadLine().getMonthValue();
			//System.out.println((double)l.get(i).getDeadLine().getMonthValue());
		}
		BarChart b = new BarChart("Frequence", "Nombres", "nbOcurence",values, 12);
		System.out.println(b);
	}
}
