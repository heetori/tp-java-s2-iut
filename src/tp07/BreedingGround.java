package tp07;

import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.HashSet;

public class BreedingGround {
	public HashSet<Participant> applicants;
	
	public boolean registration(Participant p) {
		return applicants.add(p);
	}
	
	public List<Participant> loners(){
		List<Participant> seul = new ArrayList<Participant>();
		for(Participant p : this.applicants) {
			if(!p.isMatched()) seul.add(p);
		}
		return seul;
	}
	
	public void lonersCleansing() {
		for(Participant p: this.loners()) {
			this.applicants.remove(p);
		}
	}
	public void forcedMatching() {
		Random r = new Random();
		while(this.loners().size() > 0 && this.loners().size() != 1 ) {
			int r1 = r.nextInt(this.loners().size()-1)+1;
			int r2 = r.nextInt(this.loners().size()-1)+1;
			if(r1 != r2) {
				if(!this.loners().get(r1).isMatched() && !this.loners().get(r2).isMatched()) {
					this.loners().get(r1).match(this.loners().get(r2));
				}
			}
		}
	}
	
	public List<Participant> cheaters(BreedingGround anotherBreedingGround){
		List<Participant> cheaters = new ArrayList<Participant>();
		
		for(Participant p1 : anotherBreedingGround.applicants) {
			for(Participant p2 : this.applicants) {
				if(p1.equals(p2)) {
					cheaters.add(p1);
				}
			}
		}
		return cheaters;
	}
	
	public void isolateCheaters(Participant cheater) {
		cheater.breakOff();
		for(Participant p : this.applicants) {
			if(p.getPartner().equals(cheater)) {
				p.breakOff();
			}
		}	
	}
	public void cheatersCleansing(BreedingGround anotherBreedingGround) {
		for(Participant p : this.cheaters(anotherBreedingGround)) {
			this.isolateCheaters(p);
			this.applicants.remove(p);
			anotherBreedingGround.applicants.remove(p);
		}
	}
}
