package tp07;

public class Participant {
	private String name;
	private Participant Partner;
	public Participant(String name) {
		this.name = name;
		this .Partner = null;
	}
	
	public String getName() {
		return name;
	}

	public Participant getPartner() {
		return Partner;
	}

	public String getPartnerName() {
		return this.Partner.getName();
	}	

	@Override
	public String toString() {
		return "Participant [name=" + name + ", Partner=" + Partner + "]";
	}
	
	public boolean isMatched() {
		if(this.Partner == null) {
			return false;
		}
		return true;
	}
	boolean isMatchedWith(Participant p) {
		if(this.Partner.getName() == p.getName()) {
			return true;
		}
		return false;
	}
	
	public boolean match(Participant p) {
		if(this.Partner  == null && !p.Partner.isMatched()) {
			this.Partner = p;
			p.Partner = this.Partner;
			return true;
		}
		return false;
		
	}
	public void breakOff() {
		this.Partner = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Partner == null) ? 0 : Partner.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participant other = (Participant) obj;
		if (Partner == null) {
			if (other.Partner != null)
				return false;
		} else if (!Partner.equals(other.Partner))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
