package tp08;

import java.util.Iterator;

public class Pupil_itColumnFatter implements Iterator<Pupil> {
	private Pupil[][] pupils;
	private int i = 0;
	private int j = 0;
	private int MaxWeigth = 0;
	public Pupil_itColumnFatter(Pupil[][] pupils){
		this.pupils = pupils;
	}
	
	private void findNextElement() {
		while(this.MaxWeigth < this.pupils[i][j].getWeight()) {
			if(this.i<this.pupils[0].length) {
				this.i++;
			} else {
				if(this.i < this.pupils.length) {
					this.j++;
					this.i = 0;
				}
			}
		}
		
	}

	@Override
	public boolean hasNext() {
		return this.j < this.pupils[0].length;
	}
	@Override
	public Pupil next() {
		Pupil res = this.pupils[i][j];
		this.findNextElement();
		return res;
	}
}
