package tp08;
import java.util.ArrayList;
import java.util.Iterator;

public class Line_itThreshold  implements  Iterator<Integer>{
	private ArrayList <Integer> myLine = new ArrayList<Integer>();
	private int idx;
	private int threshold;
	public Line_itThreshold(ArrayList<Integer> myLine, int threshold) {
		this.myLine.addAll(myLine);
		this.idx = 0;
		this.threshold = threshold;
		this.findNextElement();
	}
	
	private void findNextElement() {
		this.idx++;
		while(this.idx<this.myLine.size() && this.myLine.get(this.idx)<this.threshold) this.idx++;
	}
	
	@Override
	public boolean hasNext() {
		return this.idx != this.myLine.size();
	}
	
	@Override
	public Integer next() {
		Integer res = this.myLine.get(this.idx);
		this.findNextElement();
		return res;
	}
	
}

