package tp08;

import java.util.ArrayList;
import java.util.Iterator;

public class Line_itBackward implements  Iterator<Integer>{
	private ArrayList <Integer> myLine = new ArrayList<Integer>();
	private int idx;
	public Line_itBackward(ArrayList<Integer> myLine) {
		this.myLine.addAll(myLine);
		this.idx = this.myLine.size();
	}
	
	@Override
	public boolean hasNext() {
		return this.idx != 0;
	}
	
	@Override
	public Integer next() {
		if(this.idx>=0) {
			this.idx--;
		}
		Integer res = this.myLine.get(this.idx);
		return res;
	}
	
}
