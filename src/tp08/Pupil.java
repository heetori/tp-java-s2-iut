package tp08;

public class Pupil {
	private String name;
	private int weight;
	private int height;
	
	public Pupil(String name, int weight, int height) {
		this.name = name;
		this.weight = weight;
		this.height = height;
	}
	
	public String getName() {
		return name;
	}
	public int getWeight() {
		return weight;
	}
	public int getHeight() {
		return height;
	}
	
	@Override
	public String toString() {
		return "Pupil [name=" + name + ", weight=" + weight + ", height=" + height + "]";
	}
	public boolean isFatter(Pupil p) {
		if(p.getWeight() < this.getWeight()) {
			return true;
		}
		return false;
	}
	
}
