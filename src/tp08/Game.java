package tp08;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Game {
	private ArrayList<Square> gameBoard = new ArrayList<Square>();
	private ListIterator<Square> player;
	
	public Game(List<Square> gameBoard) {
		this.gameBoard.addAll(gameBoard);
		this.player = this.gameBoard.listIterator();
	}
	
	public String displayBoard() {
		String res = "";
		for(Square i : gameBoard) {
			res = res + "["+i.getID()+"] ";
		}
		return res;
	}
	
	public boolean forward(int nbTimes) {
		if(nbTimes<0) return false;
		if(gameBoard.get(nbTimes).getID()+nbTimes>gameBoard.size()) return false;
		int i = 0;
		while(i<nbTimes && player.hasNext()) player.next(); i++;
		return true;
	}
	
	public boolean backward(int nbTimes) {
		if(nbTimes<0) return false;
		if(gameBoard.get(nbTimes).getID()+nbTimes>gameBoard.size()) return false;
		int i = 0;
		while(i<nbTimes && player.hasPrevious()) player.previous(); i++;
		return true;
	}
	
	public void run() {
		
	}
}
