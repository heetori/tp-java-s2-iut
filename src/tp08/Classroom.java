package tp08;

import java.util.Random;

public class Classroom {
	private final int NB_LINE;
	private final int NB_COLUMN;
	private Pupil[][] pupils;
	
	public Classroom(Pupil[][] pupils) {
		this.NB_LINE = this.pupils.length;
		this.NB_COLUMN = this.pupils[0].length;
		this.pupils = pupils;
	}
	public Classroom(int NB_LINE, int NB_COLUM) {
		Random r = new Random();
		this.NB_COLUMN = NB_COLUM;
		this.NB_LINE = NB_LINE;
		for(int i = 0; i < NB_COLUM*NB_LINE; i++) {
			int j = r.nextInt(this.NB_LINE);
			int k = r.nextInt(this.NB_COLUMN);
			while(this.pupils[j][k] != null) j = r.nextInt(this.NB_LINE); k = r.nextInt(this.NB_COLUMN);
			pupils[j][k] = new Pupil("PersonneN"+i+"-"+j,r.nextInt(60),r.nextInt(180));
		}
	}
} 