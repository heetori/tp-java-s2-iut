package tp08;

import java.util.ArrayList;
import java.util.Iterator;


public class UseLine {
	public static void main(String [] args) {
		test_iterable();
		System.out.println();
		test_iterable_explicit();
		System.out.println();
		test_itBackward();
		System.out.println();
		test_itThresold();
	}
	public static void test_iterable() {
		ArrayList<Integer> impaire = new ArrayList<Integer>();
		for(int i = 0; i < 50; i ++) {
			if(i%2!=0) {
				impaire.add(i);
			}
		}
		Line test = new Line(impaire);
		System.out.print("test_iterable : 	 ");
		for(Integer i : test) {
			System.out.print(i +" ");
		}
	}
	public static void test_iterable_explicit() {
		ArrayList<Integer> impaire = new ArrayList<Integer>();
		for(int i = 0; i < 50; i ++) {
			if(i%2!=0) {
				impaire.add(i);
			}
		}
		Line test = new Line(impaire);
		Iterator<Integer> it = test.iterator();
		System.out.print("test_iterable_explicit : ");
		while(it.hasNext()) {
			System.out.print(it.next()+ " ");
		}
	}
	
	public static void test_itBackward() {
		ArrayList<Integer> IntArray = new ArrayList<Integer>();
		for(int i = 0; i < 50; i ++) {
			IntArray.add(i);
		}
		Iterator<Integer> it = new Line_itBackward(IntArray);
		System.out.print("test_itBackward : 	 ");
		while(it.hasNext()) {
			System.out.print(it.next()+ " ");
		}
		
	}
	
	public static void test_itThresold() {
		ArrayList<Integer> IntArray = new ArrayList<Integer>();
		for(int i = 0; i < 50; i ++) {
			IntArray.add(i);
		}
		Iterator<Integer> it = new Line_itThreshold(IntArray, 5);
		System.out.print("test_itThresold : 	 ");
		while(it.hasNext()) {
			System.out.print(it.next()+ " ");
		}
		
	}
}
