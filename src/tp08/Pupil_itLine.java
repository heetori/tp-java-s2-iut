package tp08;

import java.util.Iterator;

public class Pupil_itLine implements Iterator<Pupil> {
	private Pupil[][] pupils;
	private int i = 0;
	private int j = 0;
	public Pupil_itLine(Pupil[][] pupils){
		this.pupils = pupils;
	}
	
	private void findNextElement() {
		if(this.j<this.pupils[0].length-1) {
			this.j++;
		} else if(this.i < this.pupils.length) {
			this.i++;
			this.j = 0;
		}
	}
	@Override
	public boolean hasNext() {
		return this.i < this.pupils.length;
	}
	@Override
	public Pupil next() {
		Pupil res = this.pupils[i][j];
		this.findNextElement();
		return res;
	}

}
