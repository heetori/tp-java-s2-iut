package tp08;

public class Square implements IQuestion{
	private static int num = 0;
	private int ID;
	private int bonus = 1;
	private int penalty = 0;
	private IQuestion question;
	
	public Square(IQuestion question, int bonus, int penalty){
		this.ID = Square.num;
		Square.num++;
		this.question = question;
		this.bonus = bonus;
		this.penalty = penalty;
	}
	
	public int getID() {
		return this.ID;
	}

	public int getBonus() {
		return bonus;
	}

	public int getPenalty() {
		return penalty;
	}
	
	public boolean testAnswer(String s) {
		return question.testAnswer(s);
	}

	@Override
	public String toString() {
		 return "Square [bonus=" + bonus + ", penalty=" + penalty + ", question=" + question + "]";
	}
	
	public String displaySquare() {
		return "["+this.ID+"]";
	}
}
