package tp08;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Line implements  Iterable<Integer>{
	private ArrayList <Integer> myLine;
	
	public Line(Collection<Integer> Coll) {
		this.myLine = new ArrayList<Integer>();
		this.myLine.addAll(Coll);
	}
	
	public ArrayList<Integer> getValues(){
		return this.myLine;
	}

	@Override
	public String toString() {
		return "Line [myLine=" + myLine + "]";
	}

	@Override
	public Iterator<Integer> iterator() {
		// TODO Auto-generated method stub
		return myLine.iterator();
	}
	
}
