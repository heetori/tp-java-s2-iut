package tp08;
import java.util.Iterator;

public class UseClassroom {
	public static void main(String [] args) {
		System.out.println("COLOMN :");
		column_iteration();
		System.out.println("LINE :");
		line_iteration();
	}
	public static void column_iteration() {
		Pupil [][] pupils = new Pupil [][] {{new Pupil("Goti�1",50,50),new Pupil("Goti�2",50,50)},{new Pupil("Goti�3",50,50),new Pupil("Goti�4",50,50)},{new Pupil("Goti�5",50,50),new Pupil("Goti�6",50,50)}};
		Iterator<Pupil> it = new Pupil_itColumn(pupils);
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
	public static void line_iteration() {
		Pupil [][] pupils = new Pupil [][] {{new Pupil("Goti�1",50,50),new Pupil("Goti�2",50,50)},{new Pupil("Goti�3",50,50),new Pupil("Goti�4",50,50)},{new Pupil("Goti�5",50,50),new Pupil("Goti�6",50,50)}};
		Iterator<Pupil> it = new Pupil_itLine(pupils);
		while(it.hasNext()) {
			System.out.println(it.next());
		}
	}
	
}
