package tp08;

import java.util.Iterator;

public class Pupil_itColumn implements Iterator<Pupil> {
	private Pupil[][] pupils;
	private int i;
	private int j;
	public Pupil_itColumn(Pupil[][] pupils){
		this.pupils = pupils;
		this.i = 0;
		this.j = 0;
	}
	
	private void findNextElement() {
		if(this.i<this.pupils[0].length) {
			this.i++;
		} else {
			if(this.i < this.pupils.length) {
				this.j++;
				this.i = 0;
			}
		}
	}

	@Override
	public boolean hasNext() {
		return this.j < this.pupils[0].length;
	}
	@Override
	public Pupil next() {
		Pupil res = this.pupils[i][j];
		this.findNextElement();
		return res;
	}
}
