package tp01;
import java.util.Random;

public class RandomSequence {
	public static void main(String [] args) {
		Random r = new Random();
		if(args.length < 2 || args.length > 3) {
			System.out.println("Correct usage: <nbElt> <maxVal> [INTEGER|REAL]");
			return;
		}
		if(args.length == 2) {
			int maxVal = Integer.parseInt(args[1]);
			for(int i =0; i < Integer.parseInt(args[0]); i++) {
				System.out.println(r.nextInt(maxVal));
			}
			return;
		}
		if(args.length == 3) {
			
			if(args[2].equals("INTEGER")) {
				int maxVal = Integer.parseInt(args[1]);
				for(int i =0; i < Integer.parseInt(args[0]); i++) {
					System.out.println(r.nextInt(maxVal));
				}
				return;
			} else if(args[2].equals("REAL")) {
				int maxVal = Integer.parseInt(args[1]);
				for(int i =0; i < Integer.parseInt(args[0]); i++) {
					System.out.println(r.nextFloat() * maxVal);
				}
				return;
			}
		}
	}
}
