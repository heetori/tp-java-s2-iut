package tp01;

public class Book {
	// class attributes
	String author;
	String titre;
	int year;
	// constructor
	Book(String a, String t, int an) {
		author = a;
		titre = t;
		year = an;
	}
	// methods
	String getAuthor() {
		return author;
	}
	String getTitle() {
		return titre;
	}
	public String toString() {
		return this.getAuthor() + " " + this.getTitle() + " " +year; 
	}
}
