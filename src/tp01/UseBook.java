package tp01;

public class UseBook {
	public static void main(String[] args) {
		Book[] bibliotheque = new Book[] {
				new Book("William","Tour monde en 10 min",2018),
				new Book("Théon","La programation pour les moyens",2150),
				new Book("Gauthier","Un livre",1200)
		};
		for(int i = 0; i < bibliotheque.length; i++) {
			System.out.println(bibliotheque[i]);
		}
		
	}
}
