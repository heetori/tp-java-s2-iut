package tp05;
import util.GameBoard;
import java.util.Random;

public class UseGameBoard {
	public static void main(String[] args) {
		String[] images = new String[]{"res/sea.png", "res/grass.png", "res/dirt.png"};
		int mySize = 10;
		GameBoard p = new GameBoard(images, mySize);
		//randomInitialization(p, images.length);
		checkerBoardInitialization(p);
		p.display();
	}
	
	static void randomInitialization(GameBoard p, int nb) {
		Random r = new Random();
		int[][] tmp = p.getGame();
		for(int i =0; i < p.getNbLines(); i++) {
			for(int j = 0; j < p.getNbColumns(); j++) {
				tmp[i][j] = r.nextInt(nb)+1;
			}
		}
	}
	static void checkerBoardInitialization(GameBoard p) {
		int[][] tmp = p.getGame();
		for(int i = 0; i < p.getNbLines() ; i++) {
			for(int j = 0; j < p.getNbColumns(); j++) {
				tmp[i][j] = ((j+i)%2 == 0)?3:1;
			}
		}
	}
}
