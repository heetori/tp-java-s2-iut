package tp05;

import java.util.ArrayList;

public class Agenda {
	private ArrayList<Event> events;

	public String toString() {
		String res="";
		int nbEvent=this.events.size();
		for(int i=0;i<nbEvent;i++) {
			res=res+this.events.get(i).toString();
			res=res+"\n";
		}
		return res;
	}

	public boolean conflicting(Event evt) {
		int nbEvent=this.events.size();
		for(int i=0;i<nbEvent;i++) {
			if(this.events.get(i).overlap(evt)) return false;
		}
		return true;
	}
	public void addEvent(Event evt) {
		this.events.add(evt);
	}
	public void addEvent(Event evt,int idx) {
		this.events.add(idx, evt);
	}
	public void removeEvent(Event evt) {
		this.events.remove((Object) evt);
	}
	void removeEvent(int idx) {
		this.events.remove(idx);
	}
	void removeEvent(String label) {
		for(int i=this.events.size()-1;i>=0;i--) {
			if(this.events.get(i).label.equals(label)) this.events.remove(i);
		}
	}
	void removeOvlerapping(Event evt) {
		for(int i=this.events.size()-1;i>=0;i--) {
			if(events.get(i).overlap(evt)) this.events.remove(i); 
		}
	}
	
	void removeBetween(Event evt1, Event evt2) {
	}
}
