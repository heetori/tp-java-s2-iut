package tp05;

import java.time.LocalDate;

import tp04.Task;
import tp04.ToDoList;

public class Event {
	String label;
	String place;
	LocalDate start;
	LocalDate end;
	ToDoList tasks;
	public Event(String label,String place,LocalDate start,LocalDate end,ToDoList tasks) {
		this.label=label;
		this.place=place;
		this.start=start;
		this.end=end;
		this.tasks=tasks;
	}

	public Event(String label,String place,LocalDate start,LocalDate end) {
		this(label,place,start,end,new ToDoList());
	}

	public Event(String label,String place,LocalDate start) {
		this(label,place,start,start.plusDays(1),new ToDoList());
	}

	public Event(String label,String place) {
		this(label,place,LocalDate.now(),LocalDate.now().plusDays(1),new ToDoList());
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public LocalDate getStart() {
		return start;
	}

	public void setStart(LocalDate start) {
		this.start = start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public ToDoList getToDoList() {
		return tasks;
	}

	public void setTasks(ToDoList tasks) {
		this.tasks = tasks;
	}

	public void addTask(Task aTask) {
		this.tasks.addTask(aTask);
	}
	public void removeTask(Task aTask) {
		this.tasks.removeTask(aTask);
	}
	public void removeTask(int aTask) {
		this.tasks.removeTask(aTask);
	}
	
	public boolean equals (Object o) {
		if(this==o) return true;
		if(o==null) return false;
		if(this.getClass() != o.getClass()) return false;
		Event other=(Event) o;

		if (this.label==null) {
			if (other.label != null) return false;
		} else if(!label.equals(other.label)) return false;

		if (this.place==null) {
			if (other.place != null) return false;
		} else if(!place.equals(other.place)) return false;

		if (this.start==null) {
			if (other.start != null) return false;
		} else if(!start.equals(other.start)) return false;

		if (this.end==null) {
			if (other.end != null) return false;
		} else if(!end.equals(other.end)) return false;
		return true;
	}

	public boolean overlap(Event evt) {
		if(this.start.isAfter(evt.end)) return false;
		if(evt.start.isAfter(this.end)) return false;
		return true;
	}

	public String toString() {
		return this.label+" a lieu à "+this.place+" et commence le "+this.start.toString()+" et termine le "+this.end.toString();
	}
}
