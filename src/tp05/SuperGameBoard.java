package tp05;
import util.GameBoard;

public class SuperGameBoard {
	GameBoard p;
	public SuperGameBoard(String[] paths, int size) {
		this.p = new GameBoard(paths, size);
	}
	public void display() {
		p.display();
	}
	public int[][] getGame() {
		return p.getGame();
	}
	public int getNbColumns() {
		return p.getNbColumns();
	}
	public int getNbLines() {
		return p.getNbLines();
	}
	public void setGame(int[][] arg0) {
		p.setGame(arg0);
	}
	
	public void fillingCase(Case type, int i, int j, int[][] game) {
		this.p.getGame()[i][j] = type.ordinal()+1;
	}
	
	public void fillingDirt() {
		for(int i = 0; i < this.getNbLines(); i++) {
			for(int j = 0; j < this.getNbColumns(); j++) {
				this.fillingCase(Case.DIRT, i, j, this.getGame());
			}
		}
	}
	public void fillingSea() {
		for(int i = 0; i < this.getNbLines(); i++) {
			for(int j = 0; j < this.getNbColumns(); j++) {
				this.fillingCase(Case.SEA, i, j, this.getGame());
			}
		}
	}
	public void fillingGrass() {
		for(int i = 0; i < this.getNbLines(); i++) {
			for(int j = 0; j < this.getNbColumns(); j++) {
				this.fillingCase(Case.GRASS, i, j, this.getGame());
			}
		}
	}

	public boolean move(int x, int y, int a, int b) {
		if(x < 0 || x > this.getNbLines() && y < 0 || y > this.getNbColumns() && a < 0 || a > this.getNbLines() && b < 0 || b > this.getNbColumns()) {
			return false;
		} else {
			this.fillingCase(Case.values()[this.getGame()[x][y]], a, b, this.getGame());
			return true;
		}
	}
}
