package tp05;

public enum Case {
	
	DIRT("res/dirt.png"), SEA("res/sea.png"), GRASS("res/grass.png"), MAN("res/man.png");
	private String path;
	
	private Case(String Case){
		path = Case;
	}

	public String toString() {
		return this.path;
	}
}
