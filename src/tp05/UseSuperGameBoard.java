package tp05;

public class UseSuperGameBoard {
	public static void main(String [] args) {
		SuperGameBoard p = creationEnvironnement();
		p.fillingCase(Case.MAN, 5, 0, p.getGame());
		p.display();
	}
	
	public static SuperGameBoard creationEnvironnement() {
		String c[] = {Case.DIRT.toString(), Case.SEA.toString(), Case.GRASS.toString(), Case.MAN.toString()};
		SuperGameBoard p = new SuperGameBoard(c, 10);
		p.fillingGrass();
		for(int i = 0; i < p.getNbLines(); i ++) {
			for(int j = 3; j <=5; j++) {
				p.fillingCase(Case.SEA, i, j, p.getGame());
			}
		}
		for(int i = 4; i <= 5; i ++) {
			for(int j = 0; j < p.getNbColumns(); j++) {
				p.fillingCase(Case.DIRT, i, j, p.getGame());
			}
		}
		return p;
	}
	
}
