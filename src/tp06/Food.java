package tp06;

import java.time.LocalDate;

public class Food  implements IProduct, Comparable<Food>{
	private String label;
	private double price;
	LocalDate bestBeforeDate;
	private static int unkRef;

	public Food(String label, double price, LocalDate bestBeforeDate) {
		if(this.label == "null") {
			this.label = "refUnknown"+unkRef;
		} else {
			this.label = label;
		}
		
		this.price = price;
		this.bestBeforeDate = bestBeforeDate;
	}
	
	public Food(String label, double price) {
		this(label,price, LocalDate.now().plusDays(10));
	}
	
	public boolean isBestBefore(LocalDate aDate) {
		return this.bestBeforeDate.isBefore(aDate);
	}

	@Override
	public String toString() {
		return "["+ label + "=" + price + "-> before" + bestBeforeDate + "]";
	}

	@Override
	public double getPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isPerishable() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int compareTo(Food o) {
		return this.bestBeforeDate.compareTo(o.bestBeforeDate);
	}
	
}

