package tp06;
import java.util.ArrayList; 

public class StockAL implements ICartridgeStock {
	private ArrayList <Cartridge> stock = new ArrayList<Cartridge>();
	public StockAL() {
		
	}
	
	public StockAL(Cartridge[] c) {
		for(int i = 0; i < c.length;i++) {
			this.stock.add(c[i]);
		}
	}
	
	private int lastElt(){
		if(this.stock.get(0) == null) {
			return -1;
		}
		int i;
		for(i = 0; i < stock.size();i++) {
			if(stock.get(i) == null) {
				return i-1;
			}
		}
		return i;
	}

	@Override
	public void addCartridge(Cartridge c) {
		// TODO Auto-generated method stub
		if(this.lastElt() != this.stock.size()){
			this.stock.add(this.lastElt()+1, c);
		}
	}
	@Override
	public int getNbCartridge() {
		// TODO Auto-generated method stub
		return this.lastElt();
	}
	@Override
	public Cartridge getCartridge(int nb) {
		// TODO Auto-generated method stub
		return this.stock.get(nb);
	}

}
