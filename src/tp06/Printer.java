package tp06;

public class Printer{
	private String label;
	private String publicName;
	private PrinterType type;
	private ICartridgeStock possiblies;
	
	
	
	public Printer(String label, String publicName, PrinterType type, ICartridgeStock possiblies) {
		this.label = label;
		this.publicName = publicName;
		this.type = type;
		this.possiblies = possiblies;
	}

	public int getNbCompatibleCartridge() {
		return this.possiblies.getNbCartridge();
	}
	
	@Override
	public String toString() {
		if(this.getPossiblies() == null) {
			return this.label+":"+this.type.name() + "("+this.type.getType()+") 0";
		} else {
			return this.label+":"+this.type.name() + "("+this.type.getType()+") " + this.getPossiblies().getNbCartridge();
		}
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPublicName() {
		return publicName;
	}

	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}

	public PrinterType getType() {
		return type;
	}

	public void setType(PrinterType type) {
		this.type = type;
	}

	public ICartridgeStock getPossiblies() {
		return possiblies;
	}

	public void setPossiblies(ICartridgeStock possiblies) {
		this.possiblies = possiblies;
	}

	
	
}
