package tp06;

public interface ICartridgeStock {
	public void addCartridge(Cartridge c);
	public int getNbCartridge();
	public Cartridge getCartridge(int nb);
}
