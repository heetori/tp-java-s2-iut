package tp06;

public enum PrinterType {
	DOTMATRIX('D'),INKJET('I'),LASER('L'),DYESUBLIMATION('S'),THREED('T');
	private char type;
	
	private PrinterType(char c) {
		this.type = c;
	}

	public char getType() {
		return type;
	}
}
