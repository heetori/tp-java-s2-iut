package tp06;

public class StockTab implements ICartridgeStock {
	private Cartridge[] stock = new Cartridge [3];
	public StockTab() {
		
	}
	
	public StockTab(Cartridge[] c) {
		for(int i = 0 ; i < c.length ;i ++) {
			if(i < 3) {
				this.stock[i] = c[i];
			}
		}
	}
	
	private int lastElt(){
		if(this.stock[0] == null) {
			return -1;
		}
		int i;
		for(i = 0; i < stock.length;i++) {
			if(stock[i] == null) {
				return i-1;
			}
		}
		return i;
	}
	
	@Override
	public void addCartridge(Cartridge c) {
		// TODO Auto-generated method stub
		if(this.lastElt() != this.stock.length){
			this.stock[this.lastElt()+1] = c;
		}
	}
	@Override
	public int getNbCartridge() {
		// TODO Auto-generated method stub
		return this.lastElt();
	}
	@Override
	public Cartridge getCartridge(int nb) {
		// TODO Auto-generated method stub
		return this.stock[nb];
	}
}
