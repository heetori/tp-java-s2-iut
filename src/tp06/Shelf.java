package tp06;

import java.util.ArrayList;

public class Shelf {
	private ArrayList <IProduct> shelves = new ArrayList <IProduct>();
	private boolean refrigerated;
	private int capacityMax;
	
	public Shelf(boolean refrigerated, int capacityMax) {
		this.refrigerated = refrigerated;
		this.capacityMax = capacityMax;
	}
	
	boolean isFull() {
		return this.capacityMax <= this.shelves.size();
	}
	boolean isEmpty() {
		return this.shelves.isEmpty();
		
	}
	boolean isRefrigerated() {
		return this.refrigerated;		
	}

	@Override
	public String toString() {
		return "["+ refrigerated + ":" + capacityMax + "->"+ shelves.toString()+ "]";
	}
	
	boolean add(IProduct p) {
		if(!this.isFull()) {
			return this.shelves.add(p);
		} else {
			return false;
		}
	}
	
}
