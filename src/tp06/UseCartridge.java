package tp06;

public class UseCartridge {
	
	public static void main(String [] args) {
		Cartridge c1 = new Cartridge("P45HP-42J", "Cartouche Toner", 54.99);
		Cartridge c2 = new Cartridge("ICWCP45-3D", "Cartouche 3DInk-HP", 72.50);
		Cartridge c3 = new Cartridge("GC42-L42", "Cartouche Toner", 41.99);
		Cartridge [] c = new Cartridge [] {c1,c2,c3,c2};
		StockAL s1 = new StockAL(c);
		Printer imp1 = new Printer("HPIJ3", "HP InkJet 3" ,PrinterType.INKJET, s1);
		Printer imp2 = new Printer("HPDJ560C", "HP Deskjet 560C" ,PrinterType.LASER, null);
		System.out.println(imp1);
		System.out.println(imp2);
	}
	
}
