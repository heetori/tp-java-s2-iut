package tp06;

public class Supplies implements IProduct {
	private String label;
	private double price;
	private static int unknown;
	
	
	public Supplies(String label, double price) {
		if(this.label == "null") {
			this.label = "refUnknown"+unknown;
		} else {
			this.label = label;
		}
		this.price = price;
	}

	public String getLabel() {
		return this.label;
	}

	@Override
	public double getPrice() {
		// TODO Auto-generated method stub
		return this.price;
	}

	@Override
	public boolean isPerishable() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public String toString() {
		return "["+ label + "=" + price + "]";
	}

}
