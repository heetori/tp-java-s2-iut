package tp03;

public class Person {
	private String id;
	private String name;
	private String forename;
	
	public Person (String id, String name, String forename) {
		this.id = id;
		this.name = name;
		this.forename = forename;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getForename() {
		return forename;
	}
	public void setForename(String forename) {
		this.forename = forename;
	}
	
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", forename=" + forename + "]";
	}
	
	public boolean equals(Person otherPerson) {
		if (this == otherPerson)
			return true;
		if (otherPerson == null)
			return false;
		if (getClass() != otherPerson.getClass())
			return false;
		Person other = (Person) otherPerson;
		if (forename == null) {
			if (other.forename != null)
				return false;
		} else if (!forename.equals(other.forename))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
