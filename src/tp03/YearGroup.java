package tp03;

public class YearGroup {
	private StudentAbs yg[];

	public YearGroup(StudentAbs[] yg) {
		this.yg = yg;
	}
	
	public void addGrade(double[] aTest) {
		for(int i = 0 ; i  < this.yg.length; i++) {
			yg[i].addGrade(aTest[i]);
		}
	}
	
	public void validation(int thresholdAbs, int thresholdAvg) {
		for(int i =0; i < this.yg.length;i++) {
			if(yg[i].validation(thresholdAbs, thresholdAvg)) {
				System.out.println(yg[i].getForename() + " " + yg[i].getName());
			}
		}
	}
}
