package tp03;

import java.util.Arrays;

public class Student {
	private Person student;
	private double grades[];
	
	/*private Student(Person student, double[] grades) {
		this.student = student;
		this.grades = grades;
	}*/
	
	public Student (String id, String name, String forename, double[] grades){
		this.student = new Person(id, name, forename);
		this.grades = grades;
	}

	public String getName() {
		return student.getName();
	}

	public void setName(String name) {
		student.setName(name);
	}

	public String getForename() {
		return student.getForename();
	}

	public void setForename(String forename) {
		student.setForename(forename);
	}

	public boolean equals(Student otherStudent) {
		if (this == otherStudent)
			return true;
		if (otherStudent == null)
			return false;
		if (getClass() != otherStudent.getClass())
			return false;
		Student other = (Student) otherStudent;
		if (!Arrays.equals(grades, other.grades))
			return false;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		return true;
	}

	public String toString() {
		return "Student [student=" + student + ", grades=" + Arrays.toString(grades) + "]";
	}
	
	public double getAverage() {
		int avg = 0;
		int iMax = 0;
		for(int i =0; i < this.grades.length;i++) {
			avg += this.grades[i];
			iMax++;
		}
		avg = avg / iMax;
		return avg;
	}
	
	public void addGrade(double aGrade) {
		double[] tmpGrade = new double [this.grades.length+1];
		int iMax = 0;
		for(int i =0; i < this.grades.length;i++) {
			tmpGrade[i] = grades[i];
			iMax++;
		}
		tmpGrade[iMax+1] = aGrade;
		this.grades = tmpGrade;
		
	}
	
}
