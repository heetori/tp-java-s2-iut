package tp03;

public class StudentAbs {
	private int nbAbsence;
	private Student etu;
	
	StudentAbs(int nbAbsence, String id, String name, String forename, double[] grades) {
		this.nbAbsence = nbAbsence;
		this.etu = new Student(id,name,forename,grades);
	}
	
	public String getName() {
		return etu.getName();
	}
	public void setName(String name) {
		etu.setName(name);
	}
	public String getForename() {
		return etu.getForename();
	}
	public void setForename(String forename) {
		etu.setForename(forename);
	}
	public double getAverage() {
		return etu.getAverage();
	}
	public void addGrade(double aGrade) {
		etu.addGrade(aGrade);
	}
	
	public String toString() {
		return "StudentAbs [nbAbsence=" + nbAbsence + ", etu=" + etu + "]";
	}
	public boolean equals(StudentAbs otherStudentAbs) {
		if (this == otherStudentAbs)
			return true;
		if (otherStudentAbs == null)
			return false;
		if (getClass() != otherStudentAbs.getClass())
			return false;
		StudentAbs other = (StudentAbs) otherStudentAbs;
		if (etu == null) {
			if (other.etu != null)
				return false;
		} else if (!etu.equals(other.etu))
			return false;
		if (nbAbsence != other.nbAbsence)
			return false;
		return true;
	}
	
	public boolean warning(int thresholdAbs, double thresholdAvg) {
		if(this.nbAbsence > thresholdAbs && this.getAverage() < thresholdAvg) return true;
		return false;
	}
	public boolean validation(int thresholdAbs, double thresholdAvg) {
		if(this.nbAbsence > thresholdAbs && this.getAverage() < thresholdAvg) return true;
		return false;
	}
	
	
	
	
	
}
