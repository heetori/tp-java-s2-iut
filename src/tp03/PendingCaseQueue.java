package tp03;

public class PendingCaseQueue {
	private PendingCase[] myQueue;
	private int capacityMax;
	private int nbE;
	private int head;
	private int tail;
	
	PendingCaseQueue(int capacity){
		this.myQueue = new PendingCase[capacity];
		this.capacityMax = capacity;
		this.nbE = 0;
		this.head = 0;
		this.tail = 0;
	}
	
	public void clear() {
		for(int i = 0; i < this.myQueue.length; i++) {
			this.myQueue[i] = null;
		}
		this.head = 0;
		this.tail = 0;
		this.nbE = 0;
	}
	
	public PendingCase getPendingCase(int pos) {
		if(pos > this.capacityMax || pos < 0) return null;
		return this.myQueue[pos];
	}
	
	public boolean isEmpty() {
		return this.nbE == 0; 
	}
	public boolean isFull() {
		return this.nbE == this.capacityMax;
	}
	
	public boolean addOne(PendingCase anotherPendingCase) {
		if(this.isFull()) return false;
		this.myQueue[this.tail] = anotherPendingCase;
		this.tail = indexIncrement(this.tail);
		this.nbE++;
		return true;
	}
	
	private int indexIncrement(int idx) {
		idx++;
		if(idx >= capacityMax) idx = 0;
		return idx;
	}
	
	public int size() {
		return this.nbE;
	}
	
	public PendingCase removeOne() {
		if(this.isEmpty()) return null;
		PendingCase tmp = this.myQueue[this.head];
		this.myQueue[this.head] = null;
		this.head = indexIncrement(this.head);
		this.nbE--;
		return tmp;
	}
	
	public String toString() {
		String tmp = "";
		if(this.isEmpty()) return "";
		if(this.head > this.tail) {
			for(int i = this.head; i < this.myQueue.length;i++) {
				tmp += this.myQueue[i].toString()+ this.myQueue[i].getAmount() +"\n" ;
			}
			if(this.tail != 0) {
				for(int i = 0; i< tail;i++) {
					tmp += this.myQueue[i].toString()+ this.myQueue[i].getAmount() +"\n" ;
				}
			}
		} else {
			for(int i = this.head; i < this.tail;i++) {
				tmp += this.myQueue[i].toString()+ this.myQueue[i].getAmount() +"\n" ;
			}
		}
		return tmp;
	}
	
	public double getTotalAmout() {
		int tmp = 0;
		if(this.isEmpty()) return 0;
		for(int i = 0 ;i < this.size();i++) {
			if(this.myQueue[i] != null) tmp += this.myQueue[i].getAmount();
		}
		return tmp;
	}
}
