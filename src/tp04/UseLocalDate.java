package tp04;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import util.Keyboard;

public class UseLocalDate {
	public static void main(String args[]) {
		System.out.println(dateOfTheDay());
		System.out.println(inputDate());
		System.out.println(diffDate());
	}
	public static String dateOfTheDay() {
		LocalDate time = LocalDate.now();
		return "Today's date is "+time; 
	}	
	public static LocalDate inputDate() {		
		int year = Keyboard.readInt("Saisir votre année : ");
		int month = 0;
		int day = 0;
		while(month <= 0 || month > 12 ) {
			month = Keyboard.readInt("Saisir le mois : (compris entre 1 et 12)");
		}
		LocalDate t = LocalDate.of(year, month, 1);
		while(day <= 0|| day > t.lengthOfMonth()) {
			day = Keyboard.readInt("Saisir le jour : (compris entre 1 et "+t.lengthOfMonth()+")");
		}
		
		return LocalDate.of(year, month, day);
	}
	
	public static String diffDate() {
		System.out.println("Première date");
		LocalDate d1 = inputDate();
		System.out.println("Deuxieme date");
		LocalDate d2 = inputDate();
		if(d1.isBefore(d2)) {
			return d1.until(d2,ChronoUnit.DAYS)+"\n"+d1+"\n"+d2;
		} else {
			return d2.until(d1,ChronoUnit.DAYS)+"\n"+d2+"\n"+d1;
		}
	}
}
