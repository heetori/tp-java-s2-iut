package tp04;

public class Card {
	private Color color;
	private Rank rank;
	
	public Card(Color color, Rank rank) {
		this.color = color;
		this.rank = rank;
	}
	public Card(String color, String rank) {
		this.color = Color.valueOf(color);
		this.rank = Rank.valueOf(color);
	}
	
	public Color getColor() {
		return color;
	}
	public Rank getRank() {
		return rank;
	}
	
	public int compareRank(Card c) {
		return this.rank.compareTo(c.getRank());
	}
	public int compareColor(Card c) {
		return this.color.compareTo(c.getColor());
	}
	
	public boolean isBefore(Card c) {
		return ((this.compareColor(c) > 0) && (this.compareRank(c) > 0));
	}
	
	public boolean equals(Card c) {
		if (this == c)
			return true;
		if (c == null)
			return false;
		if (getClass() != c.getClass())
			return false;
		Card other = (Card) c;
		if (color != other.color)
			return false;
		if (rank != other.rank)
			return false;
		return true;
	}
	
	public String toString() {
		return "Card [color=" + color + ", rank=" + rank + "]";
	}
	
	
	
}
