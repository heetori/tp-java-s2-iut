package tp04;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Task {
	private static int nbTask = 0; 
	private String numid;
	private LocalDate creationDate;
	private LocalDate deadline;
	private TaskStatus state;
	private String description;
	private int duration;
	public static void main( String args[]) {
		Task t = new Task("Réussir ma vie", 1000);
		System.out.println(t);
	}
	
	public Task(String description, LocalDate l, LocalDate l2, int duration){
		Task.nbTask++;
		this.numid = (""+(LocalDate.now().getYear())%100+LocalDate.now().getMonthValue()+Task.nbTask);
		this.creationDate = l;
		this.deadline = l2;
		this.state = TaskStatus.TODO;
		this.description = description;
		this.duration = duration;
	}
	
	public Task(String description, int duration){
		Task.nbTask++;
		this.numid = (""+(LocalDate.now().getYear())%100+LocalDate.now().getMonthValue()+Task.nbTask);
		this.creationDate = LocalDate.now();
		this.deadline = this.creationDate.plus(duration, ChronoUnit.DAYS);
		this.state = TaskStatus.TODO;
		this.description = description;
		this.duration = duration;
	}
	
	
	
	public Task(LocalDate creationDate, LocalDate deadline, TaskStatus state, String description,
			int duration) {
		Task.nbTask++;
		this.numid = (""+(LocalDate.now().getYear())%100+LocalDate.now().getMonthValue()+Task.nbTask);
		this.creationDate = creationDate;
		this.deadline = deadline;
		this.state = state;
		this.description = description;
		this.duration = duration;
	}
	public LocalDate getDeadLine() {
		return this.deadline;
	}
	
	public String toString() {
		return "[T" + this.numid + " = " + this.description + ":" + this.state + "("+this.duration+")"+":"+ this.deadline+"]";
	}

	public TaskStatus getState() {
		return state;
	}

	public void changeStatus() {
		this.state = TaskStatus.values()[this.state.ordinal()+1%TaskStatus.values().length];
	}
	public void changeStatus(TaskStatus st) {
		this.state = st;
	}
	
	public void changeStatus(char c) {
		for(int i = 0;i < TaskStatus.values().length;i++) {
			if(c == TaskStatus.values()[i].toString().charAt(0) || c == TaskStatus.values()[i].toString().toLowerCase().charAt(0)){
				this.state = TaskStatus.values()[i];
				break;
			}
		}
	}
	
	public boolean isLate() {
		if(this.state == TaskStatus.FINISHED) {
			return false;
		}
		return this.deadline.isBefore(LocalDate.now());
	}
	
	public void delay(int nbDays) {
		this.deadline.plus(nbDays, ChronoUnit.DAYS);
	}
	
	
	
	
}
