package tp04;
import java.time.LocalDate;
import java.util.Arrays;
public class ToDoList {
	private Task[] chores;
	
	public ToDoList(){
		this.chores = new Task[5];
	}
	
	public void enlarge() {
		this.chores = Arrays.copyOf(this.chores, this.chores.length + 5);
	}

	public String toString() {
		return "ToDoList [chores=" + Arrays.toString(chores) + "]";
	}
	
	public void addTask(Task aTask) {
		if(this.isOverwhelmed()) 
			this.enlarge();
		for(int i = 0; i < this.chores.length;i++) {
				if(this.chores[i] == null) {
					this.chores[i] = aTask;
					return;
				}			
		}
	}
	public void removeTask(Task aTask) {
		for(int i = 0; i < this.chores.length;i++) {
			if(this.chores[i].equals(aTask)) {
				this.chores[i] = null;
				return;
			}
		}
		
	}
	public void removeTask(int i) {
		this.chores[i] = null;
	}
	
	public boolean isOverwhelmed() {
		for(int i = 0; i < this.chores.length;i++) {
			if(this.chores[i] == null) {
				return false;
			}
		}
		return true;
	}
	public int getNbTask() {
		int i;
		for(i = 0; i < this.chores.length;i++) {
			if(this.chores[i] == null) {
				return i;
			}
		}
		return i;
	}
	
	public void onSickLeave(int nbDays) {
		for(int i = 0; i < this.chores.length;i++) {
			if(this.chores[i] != null) {
				this.chores[i].delay(nbDays);
			} else {
				return;
			}
		}
	}
	
	public Task[] dueTasks() {
		LocalDate t = LocalDate.now();
		Task[] tmp = new Task[0];
		int j = 0;
		for(int i = 0; i < this.chores.length;i++) {
			if(t.equals(this.chores[i].getDeadLine())) {
				Arrays.copyOf(tmp, tmp.length+1);
				tmp[j] = this.chores[i];
				j++;
			} 
		}
		return tmp;
	}
}
