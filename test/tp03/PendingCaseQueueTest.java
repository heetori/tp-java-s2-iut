package tp03;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PendingCaseQueueTest {
	PendingCaseQueue test;
	@Before
	public void beforeATest() {
		test = new PendingCaseQueue(9);
	}
	@Test
    public void testFull() {
		for(int i = 0; i < 9; i++) {
			assertTrue(test.addOne(new PendingCase("Gauthier","got&co",102.2)));
		}
		assertTrue(test.isFull());
		assertFalse(test.addOne(new PendingCase("Gauthier","got&co",102.2)));
	}
	@Test
	public void testClear() {
		test.clear();
		assertTrue(test.isEmpty());
	}
	
	@Test
	public void testToString() {
		for(int i = 0; i < 9; i++) {
			assertTrue(test.addOne(new PendingCase("Gauthier","got&co",i)));
		}
		test.removeOne();
		test.removeOne();
		test.addOne(new PendingCase("Gauthier","got&co",102.2));
		System.out.println(test);
	}
	@Test
	public void testgetTotalAmount() {
		for(int i = 0; i < 9; i++) {
			assertTrue(test.addOne(new PendingCase("Gauthier","got&co",i)));
		}
		System.out.println(test.getTotalAmout());
	}
	
}
